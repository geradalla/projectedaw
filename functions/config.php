<?php

$usuari = "a16geradalla_m12";
$pass = "root";
$host = "labs.iam.cat";
$bd = "a16geradalla_projecte";

$con = mysqli_connect($host, $usuari, $pass, $bd);

if (!$con) {
    echo "Error: No se pudo conectar a MySQL." . PHP_EOL;
    echo "errno de depuración: " . mysqli_connect_errno() . PHP_EOL;
    echo "error de depuración: " . mysqli_connect_error() . PHP_EOL;
    exit;
}

echo "Ok" . PHP_EOL;
echo "Información del host: " . mysqli_get_host_info($con) . PHP_EOL;

mysqli_close($con);
?>
