var utils = require('utils');
var casper = require('casper').create({
  verbose: true,
  logLevel: 'error',
  pageSettings: {
    loadImages: false,
    loadPlugins: false,
    userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
  }
});
// url http://unicode.org/emoji/charts/full-emoji-list.html
var url = "taulaIcones.html";
casper.start(url, function() {
  //this.echo(this.getTitle());

  // mapeja els nodes a traves de la query del selector
  var icones_innerText = this.evaluate(function() {
    var nodes = document.querySelectorAll('table tbody tr');
    return [].map.call(nodes, function(node) {

      return node.textContent;
    });
  });


  var icones_Emoji = this.evaluate(function() {
    var nodes = document.querySelectorAll('table tbody tr');
    return [].map.call(nodes, function(node) {
      return node.childNodes[node];
    });
  });

  var icones_obj1 = icones_Emoji.map(function(str) {
    var elements = str.split("\n");
    var data = {
      icona         : elements[0].textContent

    //  'altres significats' : elements[4]
    };
    return data;
  });
  utils.dump("t1"+icones_obj1);
//document.querySelectorAll('table tbody tr')[row].childNodes[cell].innerText.trim();
  // Array d'arrays formatejat amb split
  var icones_obj = icones_innerText.map(function(str) {
    var elements = str.split("\n");
    var data = {
      id          : elements[0],
      codificacio : elements[1],
      icona       : "elements[2]",
      significat     : elements[15]
    //  'altres significats' : elements[4]
    };
    return data;
  });

  //print
  //utils.dump(icones_obj);
  var jsonIcons = JSON.stringify(icones_obj);
  //utils.dump(json);
  var fs = require('fs');

 //this.echo(JSON.stringify(icones_obj));

var val= "val;"
fs.write("icones.json", jsonIcons, 'w', 'utf8', function (err) {
      if (err) {
          return console.log(err);
      }
      console.log("The file was saved!");
  });

});



casper.run();
