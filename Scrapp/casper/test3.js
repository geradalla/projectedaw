var utils = require('utils');
var casper = require('casper').create({
  verbose: true,
  logLevel: 'error',
  pageSettings: {
    loadImages: false,
    loadPlugins: false,
    userAgent: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/29.0.1547.2 Safari/537.36'
  }
});
var url = "taulaIcones.html";
var length;

casper.start(url);

casper.then(function() {
    this.waitForSelector('table tbody tr');
});

function getCellContent(row, cell) {
    cellText = casper.evaluate(function(row, cell) {
        return document.querySelectorAll('table tbody tr')[row].childNodes[cell];
    }, row, cell);
    return cellText;
}

casper.then(function() {
    var rows = casper.evaluate(function() {
        return document.querySelectorAll('table tbody tr');
    });
    length = rows.length;
    this.echo("table length: " + length);
});

// This part can be done nicer, but it's the way it should work ...
casper.then(function() {
    for (var i = 3; i < length; i++) {
        this.echo("Date: " + getCellContent(i, 0).innerText.trim());
        //this.echo("Bid: " + getCellContent(i, 1).getAttribute("alt");
        //this.echo("Ask: " + getCellContent(i, 2).querySelector('a img').getAttribute("alt"));
        this.echo("Quotes: " + getCellContent(i, 4).textContent);
        this.echo("Quotes: " + getCellContent(i, 5).innerText);
    }
});

casper.run();
